#ifndef ADAPTATIVE_NODE_HH_
# define ADAPTATIVE_NODE_HH_

# include <utility>
# include <cstdint>
# include "utility.hh"

namespace trie
{
    /**
     * @enum NodeKind
     * @brief enum to cleanly identify each kind
     * of node. Used to make the code cleaner and
     * easier to read.
     */
    namespace NodeKind
    {
        enum
        {
            ArtNode0    = 0,
            ArtNode4    = 1,
            ArtNode16   = 2,
            ArtNode48   = 3,
            ArtNode127  = 4
        };
    }

    /**
     * @struct NodeHeader
     * @brief Information common to all node.
     *
     * type - the real type of the node :
     *  0:  NodeHeader
     *  4:  ArtNode4
     *  16: ArtNode16
     *  48: ArtNode48
     *  _:  BigNode
     */
    struct NodeHeader
    {
        uint32_t    freq;

        // Prefix
        uint32_t    offset;
        uint16_t    len: 13;

        // Node type
        uint16_t    type: 3;
    } __attribute((packed));


    /**
     * @struct GenericNode
     * @brief Structure made template to factor out
     * the common structure of ArtNode4, ArtNode16
     * and ArtNode48.
     */
    template <unsigned Size, unsigned OffsetSize = Size>
    struct GenericNode
    {
        NodeHeader  header;
        // Nexts
        char        symboles[Size];
        uint32_t    offset[OffsetSize];
    } __attribute((packed));


    /**
     * @brief specializations of the GenericNode
     * ArtNodeX : Node with a number of successor
     * less or equal to X
     */
    using ArtNode4    = GenericNode<4>;
    using ArtNode16   = GenericNode<16>;
    using ArtNode48   = GenericNode<127, 48>;

    struct ArtNode127
    {
        NodeHeader  header;
        // Nexts
        uint32_t    offset[127];
    } __attribute((packed));



    // -----------------
    // Utility functions
    // -----------------


    /**
     * @brief given the number of successors, this function
     * to the type of the node mode appropriate
     */
    uint8_t getNodeKind(uint8_t size);

    /**
     * @brief given a node, return the total size of
     * the data stored in the node. Used to know the
     * size of nodes in the dump
     */
    uint64_t getSizeOf(const NodeHeader* n);

    /**
     * @brief given the number of successors, find the type of the
     * node and then return the total size of the data stored in the
     * node. Used to know the size of nodes in the dump.
     */
    uint64_t getSizeOf(uint8_t size);

    /**
     * @brief function used to find the successord of a node, efficiently
     * depending on the type of the node.
     */
    uint64_t getNext(trie::NodeHeader* n, char c);
}


#endif /* !ADAPTATIVE_NODE_HH_ */
