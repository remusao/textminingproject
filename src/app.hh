#ifndef APP_HH_
# define APP_HH_

// Forward declaration
struct mmaped_trie;

namespace app
{
    // Forward declaration
    class DumpedTrie;


    /**
     * @brief main loop of the application, read queries
     * from the user and answer if they are correct.
     * @param trie is the trie read from the dump
     */
    void readline(const DumpedTrie& trie);


    /**
     * @brief main loop of the application in the case where
     * the input is given by an indirection of stream
     * @param trie is the trie read from the dump
     */
    void read_indirection(const DumpedTrie& map);
}

#endif /* APP_HH_ */
