#ifndef WORD_READER_HH_
# define WORD_READER_HH_

# include <fstream>
# include <iostream>
# include <string>

/**
 * @class WordReader
 * @brief Classed used to read lazyly a dictionnary of
 * words - frequencies. The main interest is to use far
 * less memory (or no memory at all), even for big files.
 */
class WordReader
{
    public:
        WordReader(const char* file);
        ~WordReader();

        // Copy - forbiden
        WordReader(const WordReader&) = delete;
        WordReader& operator=(const WordReader&) = delete;

        // Move
        WordReader(WordReader&&) = default;
        WordReader& operator=(WordReader&&) = default;

        // Methods

        /**
         * @brief check if the stream is good
         */
        bool isGood() const;

        /**
         * @brief return the next pair <word, frequency>
         */
        std::pair<std::string, uint32_t> next();

    private:
        std::ifstream  is_;
};


#endif /* !WORD_READER_HH_ */
