#include <emmintrin.h>
#include <functional>
#include "adaptativeNode.hh"

namespace trie
{

    namespace
    {
        uint64_t getNextArtNode0(NodeHeader*, char)
        {
            return 0;
        }

        uint64_t getNextArtNode4(NodeHeader* nh, char c)
        {
            ArtNode4* n = (ArtNode4*)nh;
            for (int i = 0; i < 4; ++i)
            {
                if (c == n->symboles[i])
                {
                    return n->offset[i];
                }
            }

            // No successor found
            return 0;
        }


        uint64_t getNextArtNode16(NodeHeader* nh, char c)
        {
            ArtNode16* n = (ArtNode16*)nh;
            int mask, bitfield;
            __m128i cmp;

            // Compare key `c` to the one stored in the array
            cmp = _mm_cmpeq_epi8(_mm_set1_epi8(c),
                    _mm_loadu_si128((__m128i*)n->symboles));

            // use a mask to ignore children that don't exist
            mask = (1 << n->header.type) - 1;
            bitfield = _mm_movemask_epi8(cmp) & mask;

            /*
             * * If we have a match (any bit set) then we can
             * * return the pointer match using ctz to get
             * * the index.
             * */
            if (bitfield)
                return n->offset[__builtin_ctz(bitfield)];

            return 0;
        }


        uint64_t getNextArtNode48(NodeHeader* nh, char c)
        {
            ArtNode48* n = (ArtNode48*)nh;
            if (n->symboles[(uint64_t)c] != 48)
            {
                return n->offset[(uint64_t)n->symboles[(uint64_t)c]];
            }
            return 0;
        }


        uint64_t getNextArtNode127(NodeHeader* n, char c)
        {
            return ((ArtNode127*)n)->offset[(uint64_t)c];
        }
    }



    // -----------------------------------
    // Implementation of utility functions
    // -----------------------------------

    uint8_t getNodeKind(uint8_t size)
    {
        uint8_t kind = NodeKind::ArtNode0;

        if (size == 0)
        {
            kind = NodeKind::ArtNode0;
        }
        else if (size <= 4)
        {
            kind = NodeKind::ArtNode4;
        }
        else if (size <= 16)
        {
            kind = NodeKind::ArtNode16;
        }
        else if (size <= 48)
        {
            kind = NodeKind::ArtNode48;
        }
        else
        {
            kind = NodeKind::ArtNode127;
        }

        return kind;
    }


    uint64_t getSizeOf(uint8_t size)
    {
        static uint64_t sizes[] =
        {
            sizeof (NodeHeader),
            sizeof (ArtNode4),
            sizeof (ArtNode16),
            sizeof (ArtNode48),
            sizeof (ArtNode127)
        };

        return sizes[getNodeKind(size)];
    }


    uint64_t getSizeOf(const NodeHeader* n)
    {
        static uint64_t sizes[] =
        {
            sizeof (NodeHeader),
            sizeof (ArtNode4),
            sizeof (ArtNode16),
            sizeof (ArtNode48),
            sizeof (ArtNode127)
        };

        return sizes[n->type];
    }


    uint64_t getNext(trie::NodeHeader* n, char c)
    {
        typedef uint64_t (*getNextPtr)(trie::NodeHeader*, char);
        static const getNextPtr functions[] =
        {
            getNextArtNode0,
            getNextArtNode4,
            getNextArtNode16,
            getNextArtNode48,
            getNextArtNode127
        };

        return functions[n->type](n, c);
    }
}
