#ifndef PATRICIA_TRIE_HH_
# define PATRICIA_TRIE_HH_

# include <memory>
# include <forward_list>
# include "compressedTrie.hh"

namespace trie
{
    /**
     * @struct PatNode
     * @brief node of a Patricia Trie. The main difference
     * with a CTrieNode is that the prefix in externalized into
     * an array of wchar_t, thus the node only store an offset
     * in the array and the len of the prefix.
     */
    struct PatNode
    {
        uint32_t        freq;
        uint32_t        offset;
        uint16_t        len;
        Nexts<PatNode>  nexts;
    };


    /**
     * @class Pat
     * @brief Patricia Trie
     */
    class Pat
    {
        public:
            Pat();
            ~Pat() = default;

            // Copy
            Pat(const Pat&) = delete;
            Pat& operator=(const Pat&) = delete;

            // Move
            Pat(Pat&&) = default;
            Pat& operator=(Pat&&) = default;

            // Methods

            /**
             * @brief build a Patricia Trie from a compressed trie
             */
            void build(CTrie&& t);

            /**
             * @brief check if the trie is empty
             */
            bool empty() const;

            // Accessors
            const std::unique_ptr<PatNode>& getRoot() const;
            std::unique_ptr<PatNode>& getRoot();
            std::size_t getCount() const;
            const std::string& getPrefixArray() const;
            std::size_t countNodes() const;

        private:
            std::string                 prefixArray_;
            std::size_t                 count_;
            std::unique_ptr<PatNode>    root_;
    };
}

#endif /* !PATRICIA_TRIE_HH_ */
