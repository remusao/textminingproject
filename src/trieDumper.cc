#include <assert.h>
#include <memory>
#include <unistd.h>

#include <stack>
#include <unordered_map>

#include "trieDumper.hh"
#include "dumpedTrie.hh"
#include "patriciaTrie.hh"
#include "adaptativeNode.hh"


#define CREATE_NODE(TYPE)                               \
    {                                                   \
        artNode->type = NodeKind::ArtNode##TYPE;        \
        type = TYPE;                                    \
        symboles = ((ArtNode##TYPE*)artNode)->symboles; \
        offsets  = ((ArtNode##TYPE*)artNode)->offset;   \
    }


namespace trie
{
    namespace
    {
        NodeHeader* createNode(
            const std::unordered_map<const PatNode*, uint32_t>& nodes_offsets,
            const PatNode*                                      node)
        {
            char*       symboles = nullptr;
            uint32_t*   offsets  = nullptr;
            NodeHeader* artNode  = nullptr;
            uint32_t    size = std::distance(std::begin(node->nexts), std::end(node->nexts));

            uint8_t     type = 0;
            int         index = 0;

            // ---------------
            // Create the node
            // ---------------
            switch (getNodeKind(size))
            {
                case NodeKind::ArtNode0:
                    artNode = new NodeHeader;
                    artNode->type = 0;
                    break;

                case NodeKind::ArtNode4:
                    artNode = (NodeHeader*)(new ArtNode4);
                    CREATE_NODE(4);
                    break;

                case NodeKind::ArtNode16:
                    artNode = (NodeHeader*)(new ArtNode16);
                    CREATE_NODE(16);
                    break;

                case NodeKind::ArtNode48:
                    artNode = (NodeHeader*)(new ArtNode48);
                    CREATE_NODE(48);

                    // Init to zero
                    for (int i = 0; i < 127; ++i)
                        symboles[i] = 48;

                    // Init to zero
                    for (int i = 0; i < 48; ++i)
                        offsets[i] = 0;

                    index = 0;
                    for (const auto& succ: node->nexts)
                    {
                        symboles[(uint64_t)succ.first] = index;
                        auto offsetIt = nodes_offsets.find(succ.second.get());
                        offsets[index] = offsetIt->second;
                        ++index;
                    }

                    break;

                default: // ArtNode127
                    artNode = (NodeHeader*)(new ArtNode127);
                    artNode->type = NodeKind::ArtNode127;
                    type = 127;

                    offsets  = ((ArtNode127*)artNode)->offset;
                    for (int i = 0; i < 127; ++i)
                        offsets[i] = 0;

                    for (const auto& succ: node->nexts)
                    {
                        auto offsetIt = nodes_offsets.find(succ.second.get());
                        offsets[(uint64_t)succ.first] = offsetIt->second;
                    }
                    break;
            };


            // ---------------------------------
            // Init offset and len of the prefix
            // ---------------------------------
            artNode->offset = node->offset;
            artNode->len    = node->len;
            artNode->freq   = node->freq;

            if (type != 127 && type != 48)
            {
                // --------------------------
                // Fill successors and offset
                // --------------------------
                uint64_t i = 0;
                for (const auto& succ: node->nexts)
                {
                    symboles[i] = succ.first;
                    auto offsetIt = nodes_offsets.find(succ.second.get());
                    offsets[i] = offsetIt->second;
                    ++i;
                }

                // Fill the remaining cells with 0
                for (; size < type; ++size)
                {
                    symboles[size] = 0;
                    offsets[size] = 0;
                }
            }

            return artNode;
        }


        void deleteNode(NodeHeader* node)
        {
            switch (node->type)
            {
                default:
                    delete node;
                    break;
                case NodeKind::ArtNode4:
                    delete (ArtNode4*)node;
                    break;
                case NodeKind::ArtNode16:
                    delete (ArtNode16*)node;
                    break;
                case NodeKind::ArtNode48:
                    delete (ArtNode48*)node;
                    break;
                case NodeKind::ArtNode127:
                    delete (ArtNode127*)node;
                    break;
            };
        }


        void dumpAllNodes(
            int                                                 fd,
            const std::unordered_map<const PatNode*, uint32_t>& nodes,
            const std::unique_ptr<PatNode>&                     node)
        {
            std::stack<const PatNode*>  stack;

            stack.push(node.get());

            while (!stack.empty())
            {
                auto* n = stack.top();
                stack.pop();

                // Dump the node
                auto artNode = createNode(nodes, n);
                write(fd, (char*)artNode, getSizeOf(artNode));
                deleteNode(artNode);

                // Add the successors of the node in the list
                for (const auto& succ: n->nexts)
                {
                    stack.push(succ.second.get());
                }
            }
        }


        void getAllNodes(
            std::unordered_map<const PatNode*, uint32_t>&   nodes,
            const std::unique_ptr<PatNode>&                 node)
        {
            uint32_t                    offset = 0;
            std::stack<const PatNode*>  stack;

            stack.push(node.get());

            while (!stack.empty())
            {
                auto* n = stack.top();
                stack.pop();
                nodes.emplace(n, offset);
                offset += getSizeOf(std::distance(std::begin(n->nexts), std::end(n->nexts)));

                for (const auto& succ: n->nexts)
                {
                    stack.push(succ.second.get());
                }
            }
        }
    }


    void dump(int fd, Pat&& trie)
    {
        if (trie.empty())
            return;

        // Dump size of prefixArray, and Nodes array
        app::mmaped_trie t
        {
            (uint64_t)trie.countNodes(),
            (uint64_t)trie.getPrefixArray().size()
        };
        write(fd, (char*)&t, sizeof (app::mmaped_trie));

        // Dump prefix array
        write(fd, trie.getPrefixArray().c_str(), trie.getPrefixArray().size());

        // Dump nodes
        std::unordered_map<const PatNode*, uint32_t>  nodes;

        getAllNodes(nodes, trie.getRoot());
        dumpAllNodes(fd, nodes, trie.getRoot());
    }
}
