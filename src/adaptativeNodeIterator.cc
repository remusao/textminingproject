#include "adaptativeNodeIterator.hh"
#include "adaptativeNode.hh"

namespace trie
{
    namespace
    {
        void next(std::pair<char, uint32_t>& current,
                  uint8_t&                   index,
                  const NodeHeader*          node)
        {
            switch (node->type)
            {
                default:
                    current.first = 0;
                    current.second = 0;
                    break;
                case NodeKind::ArtNode4:
                    if (index >= 4)
                    {
                        current.first = 0;
                        current.second = 0;
                    }
                    else
                    {
                        current.first = ((ArtNode4*)node)->symboles[index];
                        current.second = ((ArtNode4*)node)->offset[index];
                    }
                    break;
                case NodeKind::ArtNode16:
                    if (index >= 16)
                    {
                        current.first = 0;
                        current.second = 0;
                    }
                    else
                    {
                        current.first = ((ArtNode16*)node)->symboles[index];
                        current.second = ((ArtNode16*)node)->offset[index];
                    }
                    break;
                case NodeKind::ArtNode48:
                    for (; index < 127 && ((ArtNode48*)node)->symboles[index] == 48; ++index)
                        ;
                    if (index >= 127)
                    {
                        current.first = 0;
                        current.second = 0;
                    }
                    else
                    {
                        current.first = (char)index;
                        current.second = ((ArtNode48*)node)->offset[(uint64_t)((ArtNode48*)node)->symboles[index]];
                    }
                    break;
                case NodeKind::ArtNode127:
                    for (; index < 127 && ((ArtNode127*)node)->offset[index] == 0; ++index)
                        ;
                    if (index >= 127)
                    {
                        current.first = 0;
                        current.second = 0;
                    }
                    else
                    {
                        current.first = (char)index;
                        current.second = ((ArtNode127*)node)->offset[index];
                    }
                    break;
            };
        }
    }

    //
    // Iterators
    //


    NodeIterator::NodeIterator()
        : node_(nullptr),
          current_(std::make_pair(0, 0))
    {
    }


    NodeIterator::NodeIterator(const NodeHeader* node)
        : node_(node),
          current_(std::make_pair(0, 0))
    {
        index_ = 0;
        next(current_, index_, node_);
    }


    NodeIterator& NodeIterator::operator++()
    {
        ++index_;
        next(current_, index_, node_);
        return *this;
    }


    bool NodeIterator::operator==(const NodeIterator& node) const
    {
        return (current_.first == node.current_.first
                && current_.second == node.current_.second);
    }


    bool NodeIterator::operator!=(const NodeIterator& node) const
    {
        return !(node == *this);
    }


    const std::pair<char, uint32_t>& NodeIterator::operator*() const
    {
        return current_;
    }


    const std::pair<char, uint32_t>* NodeIterator::operator->() const
    {
        return &current_;
    }
}


namespace std
{
    trie::NodeIterator begin(const trie::NodeHeader* node)
    {
        return trie::NodeIterator(node);
    }

    trie::NodeIterator end(const trie::NodeHeader*)
    {
        return trie::NodeIterator();
    }
}
