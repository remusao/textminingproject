#ifndef COMPILER_HH_
# define COMPILER_HH_

# include <vector>
# include "compressedTrie.hh"
# include "patriciaTrie.hh"
# include "display.hh"

// Forward Declaration
class WordReader;

namespace compiler
{
    /**
     * @brief Build a compressed trie from a lazy
     * WordReader.
     * @param words lazy word reader passed by rvalue reference
     */
    trie::CTrie buildCTrie(WordReader&& words);


    /**
     * @brief build a patricia trie from a compressed trie
     * @param t a compressed trie passed by rvalue reference
     */
    trie::Pat buildPat(trie::CTrie&& t);


    /**
     * @brief utility function used to print a trie in a dot
     * format without specifying the type explicitly
     * @param t the trie we want to dump
     */
    template <typename Trie>
    Trie printTrie(Trie&& t)
    {
        display::print(std::cout, "Trie", t);
        return std::move(t);
    }
}

#endif /* !COMPILER_HH_ */
