#include <iostream>
#include <string>
#include <stack>
#include <algorithm>

#include <fcntl.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <unistd.h>

#include "dumpedTrie.hh"
#include "adaptativeNode.hh"
#include "adaptativeNodeIterator.hh"

namespace
{
    void search_impl(const app::DumpedTrie*         trie,
                     const trie::NodeHeader*        tree,
                     char                           ch,
                     char                           old_ch,
                     std::vector<Row>&              last_stack,
                     const std::string&             word,
                     std::forward_list<Result>&     res,
                     uint64_t                       max_dist,
                     std::string                    wres)
    {
        // D[i][j] is thansformed here with incremental optimization:
        // current_row = D[*][j] ;
        // last_row = D[*][j-1] ;
        // last_last_row = D[*][j-2].

        // We get the two last rows for the damerau-levenshtein.
        Row last_row = last_stack[last_stack.size() - 1];
        Row last_last_row = last_stack[last_stack.size() - 2];

        int sz = last_row.size();
        // We built our new current_row and increment the 0th element.
        Row current_row(sz);
        current_row[0] = last_row[0] + 1;

        // We update our longest prefix found.
        std::string suffix(trie->getPrefixes(), tree->offset, tree->len);
        wres = wres + suffix;

        // Calculate min cost of insertion, delete, match, transposition or substitution.
        uint64_t min = 0;

        // We need this loop as a node can contain more than one char.
        for (int c = 0; c < tree->len; ++c)
        {
            // Damerau-levenshtein distance:
            for (int i = 1; i < sz; ++i)
            {
                // Levenshtein.
                min = std::min(current_row[i - 1] + 1, last_row[i] + 1);
                min = std::min(min, (word[i - 1] == ch) ? last_row[i - 1] : (last_row[i - 1] + 1));
                // Damerau.
                if (i > 1 && old_ch != '\0' && word[i] == old_ch && word[i - 1] == ch)
                    min = std::min(min, last_last_row[i - 2] + (word[i] == ch ? 1 : 0));
                current_row[i] = min;
            }
            if (current_row[sz - 1] > max_dist)
                break;
            // We update on the total word contained by the tree.
            ch = suffix[c + 1];
            last_last_row = last_row;
            last_row = current_row;
            current_row[0] = current_row[0] + 1;
        }

        // If the cost is lower than the max_dist, then we store the result.
        if (current_row[sz - 1] <= max_dist)
        {
            // We store the result in res.
            if (tree->freq > 0)
                res.emplace_front(make_tuple(wres, tree->freq, current_row[sz - 1]));
        }

        if (*std::min_element(current_row.begin(), current_row.end()) <= max_dist)
        {
          last_stack.emplace_back(last_row);
          last_stack.emplace_back(current_row);

          // We iterate with the next trie::NodeHeader, his symbol
          // associated, the old ch, the current_row, the last_row, the
          // word, res and max_dist.
          for (const auto& it: tree)
          {
            trie::NodeHeader* n = (trie::NodeHeader*)((char*)trie->getRoot() + it.second);
            search_impl(trie, n, it.first, ch, last_stack, word, res, max_dist, wres);
          }

          last_stack.pop_back();
          last_stack.pop_back();
        }
    }
}

namespace app
{
    DumpedTrie::DumpedTrie(const char* dump)
    {
        struct stat s;

        // --
        // Opening file
        // --
        fd_ = open(dump, O_RDONLY);
        if (fd_ == -1)
        {
            std::cerr << "Unable to open dump: " << dump << std::endl;
            throw EXIT_FAILURE;
        }

        // --
        // Find size of the file
        // --
        if (fstat(fd_, &s) != 0)
        {
            std::cerr << "Unable to fstat the file: " << dump << std::endl;
            throw EXIT_FAILURE;
        }

        file_size_ = s.st_size;

        // --
        // mmap the file
        // --
        map_ = (mmaped_trie*)mmap(0, file_size_, PROT_READ, MAP_SHARED, fd_, 0);

        if (map_ == MAP_FAILED)
        {
            close(fd_);
            std::cerr << "Error while mmapping the file: " << dump << std::endl;
            throw EXIT_FAILURE;
        }

        // --
        // Init the root of the trie
        // --
        prefixes_ = (char*)((char*)map_ + sizeof (mmaped_trie));
        root_ = (trie::NodeHeader*)(prefixes_ + map_->prefix_size);
    }


    DumpedTrie::~DumpedTrie()
    {
        // --
        // un-mmap and close file descriptor
        // --
        if (munmap(map_, file_size_) == -1)
        {
            std::cerr << "Error while un-mmapping the file" << std::endl;
        }

        // --
        // Close file
        // --
        close(fd_);
    }


    uint64_t DumpedTrie::getNbNodes() const
    {
        return map_->nb_nodes;
    }


    uint64_t DumpedTrie::getPrefixSize() const
    {
        return map_->prefix_size;
    }


    const char* DumpedTrie::getPrefixes() const
    {
        return prefixes_;
    }


    const trie::NodeHeader* DumpedTrie::getRoot() const
    {
        return root_;
    }

    // Naive implementation - works
    uint32_t DumpedTrie::searchExactWord(const std::string& w) const
    {
        if (w.size() == 0)
        {
            return false;
        }

        trie::NodeHeader* current_node = root_;

        // Accumulates size of string matched
        const size_t size = w.size();
        size_t pos = 0;
        uint64_t next = 0;

        while (pos < size && (next = getNext(current_node, w[pos])))
        {
            ++pos;
            current_node = (trie::NodeHeader*)((char*)root_ + next);
        }

        return (pos == size ? current_node->freq : 0);
    }


    std::forward_list<Result>
    DumpedTrie::search(const std::string& word, uint64_t max_dist) const
    {
        std::forward_list<Result> result;
        std::string wres = "";

        // Search in the trie
        const int sz = word.size();
        Row current_row(sz + 1);

        std::vector<Row> last_stack;

        // Naive DP init.
        for (int i = 0; i <= sz; ++i)
            current_row[i] = i;

       // last_stack.emplace_back(std::vector<Row>());
        last_stack.emplace_back(current_row);
        last_stack.emplace_back(current_row);
        for (const auto& it: root_)
        {
            trie::NodeHeader* n = (trie::NodeHeader*)((char*)root_ + it.second);
            search_impl(this, n, it.first, 0, last_stack, word, result, max_dist, wres);
        }

        return result;
    }
}
