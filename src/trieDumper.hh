#ifndef TRIE_DUMPER_HH_
# define TRIE_DUMPER_HH_

namespace trie
{
    // Forward declaration
    class Pat;

    void dump(int fd, Pat&& trie);
}

#endif /* !TRIE_DUMPER_HH_ */
