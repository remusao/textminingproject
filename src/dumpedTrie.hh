#ifndef MMAP_TRIE_HH_
# define MMAP_TRIE_HH_

# include <forward_list>
# include <vector>
# include <cstdint>

// Forward declaration
namespace trie
{
    struct NodeHeader;
}

using Result = std::tuple<std::string, uint32_t, uint64_t>;
using Row = std::vector<uint64_t>;
namespace app
{
    struct mmaped_trie
    {
        uint64_t            nb_nodes;
        uint64_t            prefix_size;
    };


    class DumpedTrie
    {
        public:
            DumpedTrie(const char* dump);
            ~DumpedTrie();

            // Copy - Forbiden
            DumpedTrie(const DumpedTrie&) = delete;
            DumpedTrie& operator=(const DumpedTrie&) = delete;

            // Move
            DumpedTrie(DumpedTrie&&) = default;
            DumpedTrie& operator=(DumpedTrie&&) = default;

            // Methodes
            std::forward_list<Result> search(const std::string& w, uint64_t d) const;
            uint32_t searchExactWord(const std::string& w) const;

            // Accessors
            uint64_t getNbNodes() const;
            uint64_t getPrefixSize() const;
            const char* getPrefixes() const;
            const trie::NodeHeader* getRoot() const;

        private:
            int                 fd_;
            uint64_t            file_size_;
            mmaped_trie*        map_;
            char*               prefixes_;
            trie::NodeHeader*   root_;
    };
}

#endif /* !MMAP_TRIE_HH_ */
