.PHONY: compiler app

all: compiler app

compiler:
	make -C src/ compiler

app:
	make -C src/ app
