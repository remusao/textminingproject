#ifndef UTILITY_HH_
# define UTILITY_HH_

# include <iostream>
# include <memory>
# include <functional>

/**
 * @brief Mirror the behavior of the std::make_shared function.
 * Useful for creating unique_ptr.
 * @param args variadic arguments forwarded to the constructor of the class
 */
template<typename T, typename ...Args>
std::unique_ptr<T> make_unique(Args&& ...args)
{
    return std::unique_ptr<T>(new T(std::forward<Args>(args)...));
}

#endif /* !UTILITY_HH_ */
