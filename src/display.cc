#include <string>
#include <memory>
#include "display.hh"
#include "adaptativeNode.hh"
#include "adaptativeNodeIterator.hh"


namespace display
{
    namespace
    {
        std::size_t uniqueId()
        {
            static std::size_t id = 0;
            return id++;
        }


        void printCTrieRec(
            std::ostream& o,
            std::size_t id,
            const std::unique_ptr<trie::CTrieNode>& n)
        {
            o << id << " [label=\"" << n->prefix << "\"];" << std::endl;
            for (const auto& i: n->nexts)
            {
                std::size_t nId = uniqueId();
                o << id << " -> " << nId << ";" << std::endl;
                printCTrieRec(o, nId, i.second);
            }
        }


        void printPatRec(
            std::ostream& o,
            const std::string& pref,
            std::size_t id,
            const std::unique_ptr<trie::PatNode>& n)
        {
            std::string prefix;
            for (unsigned i = n->offset; i < (n->offset + n->len); ++i)
                prefix += pref[i];
            o << id << " [label=\"" << prefix << "\"];" << std::endl;
            for (const auto& i: n->nexts)
            {
                std::size_t nId = uniqueId();
                o << id << " -> " << nId << ";" << std::endl;
                printPatRec(o, pref, nId, i.second);
            }
        }

        void printDumpedTrieRec(
            std::ostream& o,
            const trie::NodeHeader* root,
            std::size_t id,
            const char* pref,
            const trie::NodeHeader* n)
        {
            std::string prefix;
            for (unsigned i = n->offset; i < (n->offset + n->len); ++i)
                prefix += pref[i];
            o << id << " [label=\"" << prefix << "\"];" << std::endl;

            //for (auto it = std::begin(n); it != std::end(n); ++it)
            for (const auto& it: n)
            {
                std::size_t nId = uniqueId();
                o << id << " -> " << nId << ";" << std::endl;
                printDumpedTrieRec(o, root, nId, pref, (trie::NodeHeader*)((char*)root + it.second));
            }
        }
    }


    void printSpecialized(std::ostream& o, const trie::CTrie& t)
    {
        printCTrieRec(o, uniqueId(), t.getRoot());
    }


    void printSpecialized(std::ostream& o, const trie::Pat& t)
    {
        printPatRec(o, t.getPrefixArray(), uniqueId(), t.getRoot());
    }


    void printSpecialized(std::ostream& o, const app::DumpedTrie& t)
    {
        printDumpedTrieRec(o, t.getRoot(), uniqueId(), t.getPrefixes(), t.getRoot());
    }
}
