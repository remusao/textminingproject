#ifndef DISPLAY_HH_
# define DISPLAY_HH_

# include <fstream>
# include "patriciaTrie.hh"
# include "compressedTrie.hh"
# include "dumpedTrie.hh"

namespace display
{
    /**
     * @brief Print a compressed trie in a dot format
     * @param o stream in which we dump the trie
     * @param t trie to dump
     */
    void printSpecialized(std::ostream& o, const trie::CTrie& t);

    /**
     * @brief Print a patricia trie in a dot format
     * @param o stream in which we dump the trie
     * @param t trie to dump
     */
    void printSpecialized(std::ostream& o, const trie::Pat& t);

    void printSpecialized(std::ostream& o, const app::DumpedTrie& t);


    /**
     * @brief Generic function used to factor out the header and
     * footer of the dot dump
     * @param o stream to use to dump
     * @param name name of the dot graph
     * @param t trie to dump
     */
    template <typename Trie>
    void print(std::ostream& o, const char* name, const Trie& t)
    {
        o << "digraph " << name << " {" << std::endl;
        printSpecialized(o, t);
        o << "}" << std::endl;
    }
}

#endif /* !DISPLAY_HH_ */
