#ifndef ADAPTATIVE_NODE_ITERATOR_HH_
# define ADAPTATIVE_NODE_ITERATOR_HH_

# include <cstdint>
# include <utility>

namespace trie
{
    struct NodeHeader;

    //
    // Iterators
    //

    class NodeIterator
    {
        public:
            NodeIterator();
            NodeIterator(const NodeHeader* node);

            ~NodeIterator() = default;

            // Copy
            NodeIterator(const NodeIterator&) = default;
            NodeIterator& operator=(const NodeIterator&) = default;

            // Move
            NodeIterator(NodeIterator&&) = default;
            NodeIterator& operator=(NodeIterator&&) = default;

            // Iterator methods
            NodeIterator& operator++();
            bool operator==(const NodeIterator&) const;
            bool operator!=(const NodeIterator&) const;
            const std::pair<char, uint32_t>& operator*() const;
            const std::pair<char, uint32_t>* operator->() const;

        private:
            uint8_t                     index_;
            const NodeHeader*           node_;
            std::pair<char, uint32_t>   current_;
    };
}

// Overload begin and end for iterator of Nodes
namespace std
{
    trie::NodeIterator begin(const trie::NodeHeader*);
    trie::NodeIterator end(const trie::NodeHeader*);
}


#endif /* !ADAPTATIVE_NODE_ITERATOR_HH_ */
