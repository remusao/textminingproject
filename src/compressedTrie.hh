#ifndef COMPRESSED_TRIE_HH_
# define COMPRESSED_TRIE_HH_

# include <initializer_list>
# include <memory>
# include <string>
# include <forward_list>

namespace trie
{
    /**
     * @brief type of the successors in a node. Templated on the type of nodes,
     * used to gain flexibility and lisibility in the source code
     */
    template <typename Node>
    using Nexts = std::forward_list<std::pair<char, std::unique_ptr<Node>>>;


    /**
     * @struct CTrieNode
     * @brief a node of the compressed trie. The prefix is
     * stored as a plain wstring (wide char string), and successors
     * are accessed via a forward linked list
     */
    struct CTrieNode
    {
        CTrieNode() = default;
        ~CTrieNode() = default;

        // Copy - forbiden
        CTrieNode(const CTrieNode&) = delete;
        CTrieNode& operator=(const CTrieNode&) = delete;

        // Move
        CTrieNode(CTrieNode&&) = default;
        CTrieNode& operator=(CTrieNode&&) = default;

        // Attributes
        uint32_t            freq;
        std::string         prefix;
        Nexts<CTrieNode>    nexts;
    };



    /**
     * @class CTrie
     * @brief First representation of our words in form of a trie.
     */
    class CTrie
    {
        public:
            CTrie();
            ~CTrie() = default;

            // Copy
            CTrie(const CTrie&) = delete;
            CTrie& operator=(const CTrie&) = delete;

            // Move
            CTrie(CTrie&&) = default;
            CTrie& operator=(CTrie&&) = default;

            // Methods
            bool empty() const;
            void insert(std::pair<std::string, std::size_t>&& word);
            std::size_t countNodes() const;
            void sortSuccessorsLists() const;

            // Accessors
            const std::unique_ptr<CTrieNode>& getRoot() const;
            std::unique_ptr<CTrieNode>& getRoot();
            std::size_t getCount() const;

        private:
            std::size_t                 count_;
            std::unique_ptr<CTrieNode>  root_;
    };
}

#endif /* !COMPRESSED_TRIE_HH_ */
