#include <iostream>
#include <string>
#include <limits>

#include <unistd.h>

#include "app.hh"
#include "dumpedTrie.hh"
#include "adaptativeNode.hh"
#include "display.hh"


int main(int argc, char *argv[])
{
    using namespace app;

    if (argc < 2)
    {
        // Print usage
        std::cout << "TextMiningApp usage:" << std::endl
            << argv[0] << " dump" << std::endl;
    }
    else
    {
        try
        {
            DumpedTrie trie(argv[1]);
            //display::print(std::cout, "Trie", trie);

            //--
            // Answer queries
            // --
            if (isatty(STDIN_FILENO))
            {
                readline(trie);
            }
            else
            {
                read_indirection(trie);
            }
        }
        catch (...)
        {
            std::cerr << "Application failed to load the dump. Exiting..." << std::endl;
            return EXIT_FAILURE;
        }
    }

    return 0;
}


namespace app
{
    namespace
    {
        void searchImpl(const DumpedTrie& trie, uint64_t distance, const std::string& query)
        {
            if (distance == 0)
            {
                if (trie.searchExactWord(query))
                {
                    std::cout << "Found word : " << query  << std::endl;
                }
                else
                {
                    std::cout << "Couldn't find : " << query << std::endl;
                }
            }
            else
            {
                auto res = trie.search(query, distance);

                // Print results
                auto itr = res.cbegin();
                if (itr == res.cend())
                {
                    return;
                }

                std::cout << "[{" << "\"word\":\"" << std::get<0>(*itr) << "\","
                    << "\"freq\":" << std::get<1>(*itr) << ","
                    << "\"distance\":" << std::get<2>(*itr) << "}";
                for (++itr; itr != res.cend(); ++itr)
                {
                    std::cout << "," << "{"
                        << "\"word\":" << std::get<0>(*itr) << ","
                        << "\"freq\":" << std::get<1>(*itr) << ","
                        << "\"distance\":" << std::get<2>(*itr) << "}";
                }
                std::cout << "]" << std::endl;
            }
        }
    }


    void read_indirection(const DumpedTrie& trie)
    {
        std::string     query;
        uint64_t        distance;

        while (std::cin.good())
        {
            std::cin >> query;
            if (query.compare("approx") == 0)
            {
                std::cin >> distance;
                std::cin >> query;
                searchImpl(trie, distance, query);
            }
            std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
        }
    }


    void readline(const DumpedTrie& trie)
    {
        std::string     query;
        uint64_t        distance;

        for (;;)
        {
            std::cin >> query;
            if (query.compare("approx") == 0)
            {
                std::cin >> distance;
                std::cin >> query;
                searchImpl(trie, distance, query);
            }
        }
    }
}
