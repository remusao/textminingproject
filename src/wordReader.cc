#include "wordReader.hh"


WordReader::WordReader(const char* file)
{
    is_.open(file, std::ifstream::in);
}


WordReader::~WordReader()
{
    is_.close();
}


bool WordReader::isGood() const
{
    return is_.good();
}


std::pair<std::string, uint32_t> WordReader::next()
{
    std::string     word;
    uint32_t        freq;

    is_ >> word >> freq;

    return std::make_pair(std::move(word), freq);
}

