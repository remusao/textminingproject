#include <algorithm>
#include <stack>
#include "compressedTrie.hh"
#include "utility.hh"

namespace trie
{
    CTrie::CTrie()
        : count_(0),
          root_(nullptr)
    {
    }


    // -------------------------------------------
    // Anonymous namespace to hide implementation
    // -------------------------------------------
    namespace
    {
        /**
        * @brief Create a CTrieNode, used to gain lisibility and
        * flexibility in the source code
        */
        std::unique_ptr<CTrieNode>
        createNode(std::size_t          freq,
                   const std::string&   prefix,
                   Nexts<CTrieNode>&&   nexts)
        {
            auto node = make_unique<CTrieNode>();

            node->freq = freq;
            node->prefix = prefix;
            node->nexts = std::move(nexts);

            return node;
        }


        template <typename Iterator>
        void insertRec(
            std::unique_ptr<CTrieNode>& node,
            Iterator it,
            Iterator end,
            std::size_t freq)
        {
            auto pIt = node->prefix.cbegin();
            auto pEnd = node->prefix.cend();

            // Find the longest common prefix
            while ((pIt != pEnd) && (it != end) && (*pIt == *it))
            {
                ++pIt;
                ++it;
            }

            if (pIt == pEnd)
            {
                if (it == end)
                {
                    // String and node->Prefix are equal
                    node->freq += freq;
                }
                else
                {
                    // -------------------------------
                    // String       : AB
                    // Node         :
                    //  > Prefix    : A
                    //  > Freq      : nodeFreq
                    //  > Nexts     : nodeNexts
                    //
                    // Resulting in:
                    // =============
                    //
                    //  > Case 1: there is a successor of node with the
                    //  same starting symbol than String, in which case
                    //  we recursively insert into this node.
                    //
                    //  > Case 2: there is no such a node, so we create a new
                    //  one :
                    //      > Prefix:   B
                    //      > Freq:     freq
                    //      > Nexts:    {}
                    //
                    // -------------------------------


                    // Check the presence of a successor with the same starting symbol
                    auto next = std::find_if(std::begin(node->nexts), std::end(node->nexts),
                            [&it](const std::pair<char, std::unique_ptr<CTrieNode>>& e)
                            {
                            return *it == e.first;
                            });

                    // Case 2
                    if (next == std::end(node->nexts))
                    {
                        node->nexts.emplace_front(*it, createNode(freq, std::string(it, end), Nexts<CTrieNode>()));
                    }
                    // Case 1
                    else
                    {
                        insertRec(next->second, it, end, freq);
                    }
                }
            }
            else // pIt != pEnd
            {
                if (it != end)
                {
                    // -------------------------------
                    // Breaks prefix in two nodes
                    // -------------------------------
                    // String       : AB
                    // Node:
                    //  > Prefix    : AC
                    //  > Freq      : nodeFreq
                    //  > Nexts     : nodeNexts
                    //
                    // Resulting in:
                    // =============
                    //
                    // Node 1:
                    //  > Prefix    : A
                    //  > Freq      : 0
                    //  > Nexts     : {Node 2, Node 3}
                    //
                    // Node 2:
                    //  > Prefix    : B
                    //  > Freq      : freq
                    //  > Nexts     : {}
                    //
                    //
                    // Node 3:
                    //  > Prefix    : C
                    //  > Freq      : nodeFreq
                    //  > Nexts     : nodeNexts
                    // -------------------------------

                    // Node 3
                    auto node3 = createNode(
                            node->freq,                 // Copy the old frequency
                            std::string(pIt, pEnd),     // End of the prefix (prefix - common_part)
                            std::move(node->nexts));    // move the successors

                    // Node 2
                    auto node2 = createNode(
                            freq,                       // Frequence of the word
                            std::string(it, end),       // End of the string (string - common_part)
                            Nexts<CTrieNode>());        // No successor

                    // Node 1
                    node->freq = 0;
                    // Create new prefix in two steps to avoid valgrind error
                    auto prefixTmp = std::string(node->prefix.cbegin(), pIt);
                    node->prefix = std::move(prefixTmp);
                    node->nexts = Nexts<CTrieNode>();
                    auto pIts = *pIt;
                    auto its = *it;
                    node->nexts.emplace_front(pIts, std::move(node3));
                    node->nexts.emplace_front(its, std::move(node2));
                }
                else // it == end
                {
                    // --------------------------------
                    // Breaks current node in two nodes
                    // --------------------------------
                    // String       : A
                    // Node:
                    //  > Prefix    : AB
                    //  > Freq      : nodeFreq
                    //  > Nexts     : nodeNexts
                    //
                    // Resulting in:
                    // =============
                    //
                    // Node 1:
                    //  > Prefix    : A
                    //  > Freq      : freq
                    //  > Nexts     : {Node 2}
                    //
                    // Node 2:
                    //  > Prefix    : B
                    //  > Freq      : nodeFreq
                    //  > Nexts     : nodeNexts
                    // --------------------------------


                    // TODO: Create 2 nodes
                    auto node1 = createNode(
                            node->freq,
                            std::string(pIt, pEnd),
                            std::move(node->nexts));

                    node->freq = freq;
                    node->prefix = std::string(node->prefix.cbegin(), pIt);
                    node->nexts = Nexts<CTrieNode>();
                    node->nexts.emplace_front(*pIt, std::move(node1));
                }
            }
        }


        std::size_t countNodeRec(const std::unique_ptr<CTrieNode>& node)
        {
            std::size_t count = 1;
            for (const auto& n: node->nexts)
                count += countNodeRec(n.second);
            return count;
        }
    }


    // -------------------------------------------
    // Implementation of methods
    // -------------------------------------------

    bool CTrie::empty() const
    {
        return (root_ == nullptr);
    }


    void CTrie::insert(std::pair<std::string, std::size_t>&& word)
    {
        if (word.first.size() == 0)
            return;

        // Increment counter
        ++count_;

        if (root_ == nullptr)
        {
            root_ = make_unique<CTrieNode>();
            root_->prefix = std::move(word.first);
            root_->freq = word.second;
        }
        else
        {
            insertRec(root_, word.first.cbegin(), word.first.cend(), word.second);
        }
    }


    std::size_t CTrie::countNodes() const
    {
        return countNodeRec(root_);
    }


    void CTrie::sortSuccessorsLists() const
    {
        if (root_ != nullptr)
        {
            std::stack<CTrieNode*> succ;
            succ.push(root_.get());

            while (!succ.empty())
            {
                auto node = succ.top();
                succ.pop();

                // Sort succs
                node->nexts.sort(
                [](const std::pair<char, std::unique_ptr<CTrieNode>>& a,
                   const std::pair<char, std::unique_ptr<CTrieNode>>& b)
                {
                    return (a.first < b.first);
                });

                // Push successors on stack
                for (auto& s: node->nexts)
                {
                    succ.push(s.second.get());
                }
            }
        }
    }


    std::unique_ptr<CTrieNode>& CTrie::getRoot()
    {
        return root_;
    }


    const std::unique_ptr<CTrieNode>& CTrie::getRoot() const
    {
        return root_;
    }


    std::size_t CTrie::getCount() const
    {
        return count_;
    }
}
