#include <algorithm>
#include <sstream>
#include "patriciaTrie.hh"
#include "utility.hh"

namespace trie
{
    Pat::Pat()
        : count_(0),
          root_(nullptr)
    {
    }

    // -------------------------------------------
    // Anonymous namespace to hide implementation
    // -------------------------------------------
    namespace
    {
        std::unique_ptr<PatNode> buildRec(
            std::size_t& offset,
            std::stringstream& p,
            std::unique_ptr<CTrieNode>& node)
        {
            auto res = make_unique<PatNode>();
            const auto& prefix = node->prefix;

            res->offset = offset;
            res->len = prefix.size();
            res->freq = node->freq;
            offset += res->len;

            // Copy string to vector
            p << node->prefix;

            // Build recursively
            auto nexts = std::move(node->nexts);
            // Release memory of the old node
            node.reset();

            for (auto& n: nexts)
            {
                res->nexts.emplace_front(n.first, buildRec(offset, p, n.second));
            }

            return res;
        }


        std::size_t countNodesRec(const std::unique_ptr<PatNode>& n)
        {
            std::size_t res = 1;
            for (const auto& succ: n->nexts)
            {
                res += countNodesRec(succ.second);
            }
            return res;
        }
    }


    // -------------------------------------------
    // Implementation of methods
    // -------------------------------------------


    void Pat::build(CTrie&& t)
    {
        if (t.empty())
            return;

        std::size_t offset = 0;
        std::stringstream prefixArray;

        root_ = buildRec(offset, prefixArray, t.getRoot());
        prefixArray_ = prefixArray.str();
    }


    bool Pat::empty() const
    {
        return root_ == nullptr;
    }


    std::unique_ptr<PatNode>& Pat::getRoot()
    {
        return root_;
    }


    const std::unique_ptr<PatNode>& Pat::getRoot() const
    {
        return root_;
    }


    std::size_t Pat::getCount() const
    {
        return count_;
    }

    const std::string& Pat::getPrefixArray() const
    {
        return prefixArray_;
    }

    std::size_t Pat::countNodes() const
    {
        return countNodesRec(root_);
    }
}
