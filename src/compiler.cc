#include <iostream>

#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

#include "compiler.hh"
#include "utility.hh"
#include "wordReader.hh"
#include "trieDumper.hh"


int main(int argc, char *argv[])
{
    using namespace compiler;

    if (argc != 3)
    {
        // Print usage
        std::cout << "TextMiningCompiler usage:" << std::endl
            << argv[0] << " words output" << std::endl;
    }
    else
    {
        int of = open(argv[2], O_WRONLY | O_CREAT | O_TRUNC,
                S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH);

        if (of != -1)
        {
            trie::dump(of,
                buildPat(
                buildCTrie(
                WordReader(argv[1]))));

            // printTrie(buildPat(buildCTrie(WordReader(argv[1]))));

            close(of);
        }
    }
}


namespace compiler
{
    trie::CTrie buildCTrie(WordReader&& words)
    {
        trie::CTrie ctrie;

        while (words.isGood())
        {
            ctrie.insert(words.next());
        }

        // Sort successors
        ctrie.sortSuccessorsLists();

        return ctrie;
    }


    trie::Pat buildPat(trie::CTrie&& t)
    {
        trie::Pat pat;
        pat.build(std::move(t));
        return pat;
    }
}
